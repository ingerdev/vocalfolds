from unittest import TestCase

import skimage
import numpy as np
from test_utils import create_nclass_mask
from model import data_model

from algos.mean_iou import MeanIoU
from core import data_generator


class TestMeanIoU(TestCase):
    #def create_nclass_mask_kron(self,width,height,n_classes):
     #   b = []

    def get_image_and_mask_sample(self):
        image = skimage.data.imread(r'..\dataset\img\patient2\seq6\0023_l.png')
        mask = skimage.data.imread(r'..\dataset\annot\patient2\seq6\0023_l.png')
        return image,mask

    def test_mean_iou(self):
        n_classes = 7
        image,mask = self.get_image_and_mask_sample()
        model = data_model.load_trained_model(r'..\weights.h5')
        y_pred = model.predict(np.expand_dims(image,axis=0))

        reshaped_mask = data_generator.reshape_image_to_class_array(mask, n_classes)
        miou = MeanIoU(7,weights=[0,1,1,0,0,1,1])
        calculated_miou = miou.np_mean_iou(reshaped_mask,y_pred)
        print('image miou: ',calculated_miou)
        self.assertEqual(calculated_miou,1.0)

    def test_mean_iou_checkerboard(self):
        n_classes = 7
        chessboard_mask = create_nclass_mask(512,512,7)
        #chessboard_array = data_generator.reshape_image_to_class_array(chessboard_mask,7)
        reshaped_mask = data_generator.reshape_image_to_class_array(chessboard_mask, n_classes)
        miou = MeanIoU(7,weights=[1,1,1,1,1,1,1])
        calculated_miou = miou.np_mean_iou(reshaped_mask,reshaped_mask)
        print('checkerboard miou: ',calculated_miou)
        self.assertEqual(calculated_miou,1.0)
