import numpy as np

def create_nclass_mask(width,height,n_classes):
    """
    Generate chessboard-like 2d image array
    where every cell labeled by one of n_classes
    cells are labeled in such order:
    0,1,2,3...n
    1,2,3,4...0
    2,3,4,5...1
    ...
    n,n-1,n-2...0
    note that cells itself has its size (width/n_classes,height/cell_classes)
    also note remainders of width or height divided to n_classes wont be filled and are zeroed
    (it is right and bottom matrix areas)
    :param width: width of image
    :param height: height of image
    :param n_classes: total number of classes
    :return: 2d mask array
    """
    mask = np.zeros((width,height))
    classes=range(n_classes)
    slice_height = int(height/n_classes)
    slice_width = int(width/n_classes)
    for row in range(n_classes):
        for col in range(n_classes):
            mask[row*slice_height:(row+1)*slice_height,
            col*slice_width:(col+1)*slice_width] = classes[row-col]

    return mask