from unittest import TestCase
from . import test_utils
from core import data_generator
from model import vocalfolds,data_model
import matplotlib.pyplot as plt;
import skimage;
import numpy as np

class TestPredictionImage(TestCase):

    def _test_checkers_array(self):
        chessboard_mask = test_utils.create_nclass_mask(512,512,7)
        chessboard_array = data_generator.reshape_image_to_class_array(chessboard_mask,7)
        restored_mask = data_generator.reshape_prediction_array_to_mask_image(chessboard_array,(512,512))

        fig, (true_mask_plot,restored_mask_plot) = plt.subplots(2,1,figsize=(6,15));
        #combined_masks.imshow(skimage.color.label2rgb(mask,original_image,['black','red','yellow','gray','blue','cyan']))
        true_mask_plot.imshow(chessboard_mask)
        true_mask_plot.imshow(chessboard_mask)
        restored_mask_plot.imshow(restored_mask)
        fig.show();
        self.assertEqual(1 == 0)

    def test_score_on_training(self):
        x_filenames,y_filenames =  vocalfolds.VocalFolds(r'..\dataset\img',
                                                         r'..\dataset\annot')
        model = data_model.load_trained_model(r'C:\Projects\Home\Ingerdev\TestPad\redox\vocalfolds\weights.h5')

        for x_filename,y_filename in zip(x_filenames,y_filenames):
            #convert file name to
            x_image = skimage.data.imread(x_filename)
            y_image = skimage.data.imread(y_filename)

            #convert (W,H) mask to (W*H,n_classes) mask
            y = data_generator.reshape_image_to_class_array(y_image,7)
            y = np.expand_dims(y,axis=0)
            y_pred = model.evaluate(np.expand_dims(x_image,axis=0),y)
            return