import numpy as np
import skimage;
from core import data_util


class VocalFolds:
    #images and masks containing folders
    _image_folder = ''
    _mask_folder = ''

    #fully-qualified file names for images and masks
    _images = np.asarray([])
    _masks = np.asarray([])

    def __init__(self,image_folder,mask_folder):
        self._image_folder = str(image_folder)
        self._mask_folder = str(mask_folder)

        #iterate over image_folder and collect full image names
        self._images = np.asarray(data_util.enum_files(self._image_folder))
        #exploit strictly the same folder structure and file names for images-related masks
        #and compose masks list without psysical iterations over filesystem
        self._masks = np.array([mask_folder + image[len(image_folder):] for image in self._images],dtype=str)

    def X(self):
        return self._images
    def Y(self):
        return self._masks
    def parse_files(self,X_filename,y_filename):
        """

        :param X_file: X file name
        :param y_file: y file name
        :return: parsed images X and y
        """
        x_image = skimage.data.imread(X_filename)
        y_image = skimage.data.imread(y_filename)
        return

    #create ability to use class like x,y = vocalfolds(...)
    def __iter__(self):
        return (i for i in (self._images, self._masks))

