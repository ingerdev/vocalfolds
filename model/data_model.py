import keras
from keras.layers import Conv2D, MaxPool2D, Conv2DTranspose,Activation
from keras.optimizers import Adam
from algos.lovasz_loss import keras_lovasz_softmax_classes_1_6
from algos.mean_iou import MeanIoU
from algos.weighted_cross_entropy import weighted_categorical_crossentropy
from  keras.losses import categorical_crossentropy


smooth = 1.
def dice_coef(y_true, y_pred):
    y_true_f = keras.backend.flatten(y_true)
    y_pred_f = keras.backend.flatten(y_pred)
    intersection = keras.backend.sum(y_true_f * y_pred_f)
    return (2. * intersection + smooth) / (keras.backend.sum(y_true_f) + keras.backend.sum(y_pred_f) + smooth)


def dice_coef_loss(y_true, y_pred):
    return -dice_coef(y_true, y_pred)

# create two convolute layers with maxpool one
# convolute layers has fixed kernel size of 3x3
# max_pool layer has fixed kernel size of 2x2
# with_max_pool_layer parameter defines should we add max_pool layer or not
# return latest convolute layer and max pool layer
def path_down(input_layer, filters_count, with_max_pool_layer=True):
    conv_layer = Conv2D(filters_count, (3, 3), padding='same', activation='relu')(input_layer)
    conv_layer = Conv2D(filters_count, (3, 3), padding='same', activation='relu')(conv_layer)
    max_pool_layer = MaxPool2D(pool_size=(2, 2),padding='same')(conv_layer) if with_max_pool_layer else None

    return conv_layer, max_pool_layer

def path_up(bottom_layer,side_layer,filters_count):
   #up6 = concatenate([Conv2DTranspose(256, (2, 2), strides=(2, 2), padding='same')(conv5), conv4], axis=3)
   #test = Conv2DTranspose(filters_count, (2, 2), strides=(2, 2), padding='same')(bottom_layer)

   union_layer = keras.layers.concatenate([Conv2DTranspose(filters_count, (2, 2), strides=(2, 2), padding='same')(bottom_layer), side_layer], axis=3)
   conv_layer = Conv2D(filters_count, (3, 3), padding='same', activation='relu')(union_layer)
   conv_layer = Conv2D(filters_count, (3, 3), padding='same', activation='relu')(conv_layer)

   return conv_layer


#u-net segmentation model implementation
#based on this article
#https://arxiv.org/pdf/1505.04597.pdf

##task-specific classes:
#0 - background area
#1 - intubation area
#2 - tumor area
def Model(image_shape,classes_number):
    input_layer = keras.layers.Input(shape=(image_shape))

    conv_layer_1, max_pool_layer_1 = path_down(input_layer, 64)
    conv_layer_2, max_pool_layer_2 = path_down(max_pool_layer_1, 128)
    conv_layer_3, max_pool_layer_3 = path_down(max_pool_layer_2, 256)
    conv_layer_4, max_pool_layer_4 = path_down(max_pool_layer_3, 512)

    conv_layer_5, _ = path_down(max_pool_layer_4,1024, False)

    conv_layer_6 = path_up(conv_layer_5,conv_layer_4,512)
    conv_layer_7 = path_up(conv_layer_6,conv_layer_3,256)
    conv_layer_8 = path_up(conv_layer_7,conv_layer_2,128)
    conv_layer_9 = path_up(conv_layer_8,conv_layer_1,64)
    #print(conv_layer_9)
    #sigmoid prediction layer
    classes_conv_layer = Conv2D(classes_number,(1,1),padding='same',activation='relu')(conv_layer_9)
    #reshaped_class_layer = keras.layers.Reshape((classes_number,image_shape[0] * image_shape[1]))(classes_conv_layer)
    reshaped_class_layer = keras.layers.Reshape((image_shape[0] * image_shape[1],classes_number))(classes_conv_layer)
    #permuted_class_layer = keras.layers.Permute((2, 1))(reshaped_class_layer)

    convergence_layer = Activation('softmax')(reshaped_class_layer)


    model = keras.Model([input_layer],[convergence_layer],name="model_zero")
    return model

def load_trained_model(weights_path,weights):
    model = gen_model_crossentropy_loss(weights)#gen_model_lovasz_loss()
    model.load_weights(weights_path)
    return model

def gen_model_crossentropy_loss(weights):
    width = 512
    height=512
    classes_number = 7
    constructed_model = Model((width,height,3),classes_number)

    loss = weighted_categorical_crossentropy(weights = weights) #dice_coef_loss#
    miou_metric = MeanIoU(classes_number,weights=[0,1,1,1,1,1,1])


    constructed_model.compile(optimizer=Adam(lr=1e-5),loss=loss,metrics=[miou_metric.mean_iou,dice_coef])
    #model.compile(optimizer=Adam(lr=1e-5),loss=dice_coef_loss,metrics=[dice_coef])
    return constructed_model

def gen_model_dice_loss(weights):
    width = 512
    height=512
    classes_number = 7
    constructed_model = Model((width,height,3),classes_number)

    loss = weighted_categorical_crossentropy(weights = weights) #dice_coef_loss#
    miou_metric = MeanIoU(classes_number,weights=weights)


    constructed_model.compile(optimizer=Adam(lr=1e-4),loss=dice_coef_loss,metrics=[miou_metric.mean_iou,dice_coef])
    #model.compile(optimizer=Adam(lr=1e-5),loss=dice_coef_loss,metrics=[dice_coef])
    return constructed_model

def gen_model_crossentropy_sgd():
    width = 512
    height=512
    classes_number = 7
    constructed_model = Model((width,height,3),classes_number)

    weights = [0.,1.,1.,1.,10.,1.,1.]
    loss = weighted_categorical_crossentropy(weights = weights) #dice_coef_loss#
    miou_metric = MeanIoU(classes_number,weights=weights)


    constructed_model.compile(optimizer=Adam(lr=1e-5),loss=loss,metrics=[miou_metric.mean_iou])
    #model.compile(optimizer=Adam(lr=1e-5),loss=dice_coef_loss,metrics=[dice_coef])
    return constructed_model


def gen_model_lovasz_loss():
    width = 512
    height=512
    classes_number = 7
    constructed_model = Model((width,height,3),classes_number)
    miou_metric = MeanIoU(classes_number)

    constructed_model.compile(optimizer=Adam(lr=1e-5),loss=keras_lovasz_softmax_classes_1_6,metrics=[miou_metric.mean_iou])
    return constructed_model










