from keras_preprocessing.image import ImageDataGenerator
import pandas as pd
import numpy as np
import skimage
from core import data_generator
import imgaug as ia
from imgaug import augmenters as iaa
"""
class augmented_data_generator:
    def __init__(self,**image_gen_args):
        self.image_gen = ImageDataGenerator(image_gen_args)
        pass

    def generate(self,data,n_classes,batch_size=6,flatten=False):       
        return
"""
def mask_add_depth(mask,n_classes):
    """
    :param mask: input mask WxH contained regions from range(n_classes)
    :param n_classes: total tumber of classes
    :return: WxHxn_classes mask where each of n_classes' WxH layer contain pixels from related class
    """
    deep_mask = np.zeros((mask.shape[0],mask.shape[1],n_classes))
    for class_id in range(n_classes):
        deep_mask[:,:,class_id] = (mask == class_id).astype(int)
    return deep_mask


def flatten_depth_mask(deep_mask,n_classes):
    """
    Reshape 3d mask to 2d mask with shape (total_pixels,n_classes)
    :param deep_mask: input mask(W,H,C)
    :param n_classes: defines C - classes count and depth of deep_mask
    :return: reshaped mask (WxH,C)
    """
    return np.reshape(deep_mask,(deep_mask.shape[0]*deep_mask.shape[1],n_classes))


def reshape_image_to_class_array(mask,n_classes):
    """
    reshape 2d mask (W,H) with pixel values defines classes to
    binary 2d mask (W*H,C), where C - classes count
    :param mask: input (W,H) mask
    :param n_classes: C,total count of possible classes (some classes may be not present
    in particular mask)
    :return: binary-coded mask (W*H,C)
    """
    deep_mask = mask_add_depth(mask,n_classes)
    return flatten_depth_mask(deep_mask,n_classes)

def reshape_prediction_array_to_mask_image(array,shape):
    """
    reshape (W*H,n_classes) array to (W,H) grayscale image
    :param array: input array of shape (W*H,n_classes)
    :param shape: output array shape, (W,H)
    :return: restored mask grayscale image (W,H)
    """
    array = np.argmax(array, axis=-1).ravel()
    return np.reshape(array,shape)

def flatten_class_image_batch(image_generator, n_classes):
    """
    Generate batch of reshaped masks (batch,W*H,C)
    batch count defined inside image_generator
    :param image_generator: generate batches of images
    :param n_classes: C, total number of classes
    :return: batch of flattened masks (batch,W*H,C)
    """
    while True:
        flattened_images = []
        for image in next(image_generator):
            flattened_images.append(reshape_image_to_class_array(image[:,:,0],n_classes))
        yield np.stack(flattened_images,axis=0);

def imgaug_image_generator(X,y,X_images,y_images,n_classes,batch_size=6,random_state=None):
    assert(len(X)==len(y))

    X_length = len(X)
    #default augmentation arguments

    seq = iaa.Sequential([
        iaa.Sometimes(0.1,iaa.Sharpen((0.0, 1.0))),       # sharpen the image
        # In some images move pixels locally around (with random
        # strengths).
        iaa.Sometimes(0.2,
            iaa.ElasticTransformation(alpha=(0.5, 3.5), sigma=0.25)
        ),

        iaa.Fliplr(0.5), # horizontal flips
        iaa.Flipud(0.2), # vertical flips
        iaa.Crop(percent=(0, 0.1)), # random crops
        # Small gaussian blur with random sigma between 0 and 0.5.
        # But we only blur about 50% of all images.
        iaa.Sometimes(0.3,# Blur each image with varying strength using
                      # gaussian blur (sigma between 0 and 3.0),
                      # average/uniform blur (kernel size between 2x2 and 7x7)
                      # median blur (kernel size between 3x3 and 11x11).
                      iaa.OneOf([
                          iaa.GaussianBlur((0, 3.0)),
                          iaa.AverageBlur(k=(2, 7)),
                          iaa.MedianBlur(k=(3, 11)),
                      ])
                      ),
        # Either drop randomly 1 to 10% of all pixels (i.e. set
        # them to black) or drop them on an image with 2-5% percent
        # of the original size, leading to large dropped
        # rectangles.
        iaa.OneOf([
            iaa.Dropout((0.01, 0.1), per_channel=0.5),
            iaa.CoarseDropout(
                (0.03, 0.15), size_percent=(0.02, 0.05),
                per_channel=0.2
            ),
        ]),
        # Strengthen or weaken the contrast in each image.
        iaa.ContrastNormalization((0.75, 1.5)),
        # Add gaussian noise.
        # For 50% of all images, we sample the noise once per pixel.
        # For the other 50% of all images, we sample the noise per pixel AND
        # channel. This can change the color (not only brightness) of the
        # pixels.
        iaa.AdditiveGaussianNoise(loc=0, scale=(0.0, 0.05*255), per_channel=0.5),
        # Make some images brighter and some darker.
        # In 20% of all cases, we sample the multiplier once per channel,
        # which can end up changing the color of the images.
        iaa.Multiply((0.8, 1.2), per_channel=0.2),
        # Apply affine transformations to each image.
        # Scale/zoom them, translate/move them, rotate them and shear them.
        iaa.Affine(
            scale={"x": (0.8, 1.2), "y": (0.8, 1.2)},
            translate_percent={"x": (-0.2, 0.2), "y": (-0.2, 0.2)},
            rotate=(-5, 5),
            shear=(-8, 8)
        )
    ], random_order=True) 



    """
    X_images = []
    y_images = []
    import cv2
    for x_filename,y_filename in X,y:
        x_image = cv2.imread(x_filename)
        y_image = cv2.imread(x_filename)
        X_images.append(x_image)
        y_images.append(y_image)
    """
    batch_x = np.zeros((batch_size, 512, 512, 3),dtype=np.float)
    batch_y = np.zeros((batch_size, 512*512,7),dtype=np.uint8)
    while True:
        import matplotlib.pyplot as plt;

        random_sample_indices = np.random.choice(X_length,batch_size)
        batch_row_index = 0
        for x_sample_image_name,y_sample_image_name in zip(X[random_sample_indices],y[random_sample_indices]):
            seq_det = seq.to_deterministic()
            batch_x[batch_row_index] = seq_det.augment_image(X_images[x_sample_image_name])/255.0

            y_image = y_images[y_sample_image_name]
            mask = ia.SegmentationMapOnImage(y_images[y_sample_image_name], shape=(512,512), nb_classes=n_classes)
            mask = seq_det.augment_segmentation_maps([mask])[0].get_arr_int()
            batch_y[batch_row_index] = reshape_image_to_class_array(mask[:,:],n_classes)
            """
            restored_mask = data_generator.reshape_prediction_array_to_mask_image(batch_y[batch_row_index],(512,512))
            fig, (image_plot,mask_plot) = plt.subplots(2,1,figsize=(6,15));

            image_plot.imshow(batch_x[batch_row_index])
            mask_plot.imshow(restored_mask)
            fig.show();
            """
            batch_row_index +=1


        yield batch_x,batch_y



def augmented_image_generator(X,y,n_classes,batch_size=6,random_state=None,**data_gen_args):
    """

    :param X: original data image, (W,H,3), rgb-coded
    :param y: related mask (W,H), class-coded aka "grayscale"
    :param n_classes: C, defines existent classes count
    :param batch_size: batch size  of generated images
    :param random_state: seed for random image permutations
    :param data_gen_args: predefined set of ImageDataGenerator args
    :return: generator which will generate batches of generated data
    """
    default_data_gen_args = {
        'horizontal_flip':True,
        'vertical_flip':True,
        'width_shift_range':0.1,
        'height_shift_range':0.1,
        'shear_range':0.1,
        'zoom_range':0.1,
        'fill_mode':'reflect',
        #'cval':0,
        'rotation_range':180,
    }
    if (not data_gen_args):
        data_gen_args = default_data_gen_args

    shuffle = True if random_state!=None else False
    shuffle = False


    X_df = pd.DataFrame({'filename':X})
    trans_gen = ImageDataGenerator(**data_gen_args)

    x_gen = ImageDataGenerator(rescale=1.0/255)

    x_flow = x_gen.flow_from_dataframe(X_df,target_size=(512,512),
                                       batch_size=batch_size,shuffle=shuffle,seed=random_state,drop_duplicates=False,class_mode=None)

    y_df = pd.DataFrame({'filename':y})
    y_gen = ImageDataGenerator()
    y_flow = y_gen.flow_from_dataframe(y_df,target_size=(512,512),color_mode='grayscale',
                                       batch_size=batch_size,shuffle=shuffle,seed = random_state,drop_duplicates=False,class_mode=None)
    while True:
        x_batch = next(x_flow)
        y_batch = next(y_flow)
        transformed_x = np.zeros((x_batch.shape[0], 512, 512, 3))
        transformed_y = np.zeros((y_batch.shape[0], 512*512,7))
        for k in range(x_batch.shape[0]):
            transformation = trans_gen.get_random_transform((512,512),random_state)

            transformed_x[k] = trans_gen.apply_transform(x_batch[k],transformation)
            transformed_x[k] = skimage.util.random_noise(transformed_x[k],mode='s&p')

            y = trans_gen.apply_transform(y_batch[k],transformation)
            transformed_y[k] = reshape_image_to_class_array(y[:,:,0],n_classes)
        yield transformed_x,transformed_y

    #flattened_y_flow = flatten_class_image_batch(y_flow,n_classes)
    #return zip(x_flow,flattened_y_flow)

