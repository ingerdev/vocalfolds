import skimage;
import numpy as np

def calculate_dataset_wide_weights(y,ignored_classes):
    """
    For each mask file in y load it, calculate class-label pixels
    and sum with overall statistic, then return
    inversed array of probilities 1/frequences
    :param y: mask images file list
    :param ignored_classes: classes that should be ignored
    :return: array [0..class_count-1) with weights
    """

    classes = {}
    for mask_filename in y:
        mask = skimage.data.imread(mask_filename)
        labels,count =  np.unique(mask,return_counts=True)
        for label,label_count in zip(labels,count):
            if label in classes:
                classes[label] += label_count
            else:
                classes[label] = label_count

    total_pixels = sum(classes[class_label] for class_label in range(len(classes)) if class_label not in ignored_classes)
    return [total_pixels/classes[class_label] if class_label not in ignored_classes else 0 for class_label in range(len(classes))]