from model import vocalfolds,data_model
from core import data_generator
import matplotlib.pyplot as plt;
from core import pipeline,training
import numpy as np
from tests import test_utils
from algos.mean_iou import MeanIoU
import skimage;

def show_prediction(image,true_mask,pedicted_mask):
    fig, (orig,true_mask_plot,predicted_mask_plot,combined_masks) = plt.subplots(4,1,figsize=(6,15));
    #combined_masks.imshow(skimage.color.label2rgb(mask,original_image,['black','red','yellow','gray','blue','cyan']))
    orig.imshow(image)
    true_mask_plot.imshow(true_mask)
    predicted_mask_plot.imshow(pedicted_mask)
    #fig.colorbar()
    fig.show();
    return

def print_iou(y_true,y_pred):
    miou = MeanIoU(7)#,weights=[0,1,1,0,0,1,1])
    calculated_miou = miou.np_mean_iou(y_true,y_pred)
    print(calculated_miou)

def test_checkers_array():
    chessboard_mask = test_utils.create_nclass_mask(512,512,7)
    chessboard_array = data_generator.reshape_image_to_class_array(chessboard_mask,7)
    restored_mask = data_generator.reshape_prediction_array_to_mask_image(chessboard_array,(512,512))

    fig, (true_mask_plot,restored_mask_plot) = plt.subplots(2,1,figsize=(6,15));
    #combined_masks.imshow(skimage.color.label2rgb(mask,original_image,['black','red','yellow','gray','blue','cyan']))
    true_mask_plot.imshow(chessboard_mask)
    restored_mask_plot.imshow(restored_mask)
    fig.show();
    return

def test_augmented_pipeline():

    multiplicator = pipeline.Multiplicator(n_mult=10,shuffle=True,random_state=500)
    devectorizer = pipeline.Devectorizer(X,y)

    train_flow =  pipeline.augmented_image_generator(X_p_train,y_p_train,n_classes=7,batch_size=3,random_state=train_random_state)
    test_flow = pipeline.augmented_image_generator(X_p_test,y_p_test,n_classes=7,batch_size=3,random_state=test_random_state)

    return
def main():
    #test_checkers_array()
    x_filenames,y_filenames =  vocalfolds.VocalFolds(r'..\dataset\img',
                                r'..\dataset\annot')
    model = data_model.load_trained_model(r'..\weights.h5',list(range(7)))
    #
    for x_filename,y_filename in zip(x_filenames,y_filenames):

        #convert file name to
        x_image = skimage.data.imread(x_filename)
        y_image = skimage.data.imread(y_filename)

        #convert (W,H) mask to (W*H,n_classes) mask
        y = data_generator.reshape_image_to_class_array(y_image,7)
        y = np.expand_dims(y,axis=0)
        y_pred = model.predict(np.expand_dims(x_image,axis=0))

        #convert y_pred back to WxH image
        y_pred_image = data_generator.reshape_prediction_array_to_mask_image(y_pred,(512,512))

        show_prediction(x_image,y_image,y_pred_image)
        print_iou(y,y_pred)
    return


if __name__ == "__main__":
    main()