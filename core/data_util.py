import skimage;
import scipy
import matplotlib.pyplot as plt;
import numpy as np;
import os


#return image combined with image and mask
#image param - input rgb image
#mask param - 0/1 mask
#returned image has amplified green channel x2
def lay_mask_over_image(image,mask):
    return


# for each pixel labeled as "class_to_remove"
# change it in such order:
# scan 3x3 area around center pixel, find
# most frequent label here (except class_to_remove)
# and change center pixel's label to most frequent one.
def filter_3x3_func(input,class_to_remove):
    #get filtered pixel (center pixel)
    center_label = input[4]

    #if filtered element isnt our label - leave it untouched
    if (center_label != class_to_remove):
        return center_label

    #sort uniques and their frequency
    unique_labels, counts = np.unique(input,return_counts=True)
    #list of tuples (label,number of occurences in 3x3 area)
    label_info = sorted(zip(unique_labels, counts),reverse=True,key = lambda t: t[1])

    #we didnt find any adjacent pixel with label another that class_to_remove
    #dont change center pixel
    if (len(label_info) == 1 and label_info[0][0] == class_to_remove):
        return center_label

    #change center pixel label to label with most occurences (if it is class_to_remove label
    #then select second most frequent class)
    return label_info[0][0] if label_info[0][0]!=class_to_remove else label_info[1][0]


#in the given mask remove pixels with label "class_to_remove"
#replacing their value with neighboring labels values
def remove_class_inpaint_manual(mask,class_to_remove,maximum_treshold=100):
    updated = np.copy(mask)

    #call filter proc until no single class_to_remove labeled pixels
    #will be found in the mask array
    while (np.any(updated[:,:] == class_to_remove)):
        updated = scipy.ndimage.generic_filter(updated,filter_3x3_func,mode='reflect',
                                               size=(3,3),extra_arguments=(class_to_remove,))
    return updated

def convert_masks(input_folder,output_folder, create_output_folder=True):
    import os

    if not os.path.isdir(output_folder):
        os.mkdir(output_folder)

    for subdir, _, files in os.walk(input_folder):
        #note that os.path.join will interpret any subdirs started with / as
        #start of absolute path and will throw away anything it collected before
        current_output_dir = os.path.join(output_folder, subdir[len(input_folder):].lstrip(os.sep))
        print(output_folder,subdir[len(input_folder):],current_output_dir)
        if not os.path.isdir(current_output_dir):

            os.mkdir(current_output_dir)

        for file in files:
            input_mask = skimage.data.imread(os.path.join(subdir, file))
            print(file, np.unique(input_mask,return_counts=True))
            #output_mask = remove_class_inpaint_manual(input_mask,0)
            #import scipy.misc
            #scipy.misc.imsave(os.path.join(current_output_dir, file),output_mask)

def enum_files(folder):
    raw_filenames = []
    for subdir, _, files in os.walk(folder):
        raw_filenames.extend(os.path.join(subdir, file) for file in files)

    return raw_filenames

def enum_classes(mask_filename):
    mask = skimage.data.imread(mask_filename)
    class_labels,pixels_counts = np.unique(mask,return_counts=True)

    return list(zip(class_labels,pixels_counts))

#mask_filenames - array [...] with filename strings
def count_classes(mask_filenames):
    raw_classes = []
    shapes = []
    for mask_file in mask_filenames:
        input_mask = skimage.data.imread(mask_file)
        shapes.append(input_mask.shape)
        raw_classes.extend(np.unique(input_mask))
    class_marks,class_counts = np.unique(raw_classes,return_counts=True)
    return zip(class_marks,class_counts)#sorted(zip(class_marks,class_counts),key=lambda t:t[1])



def test_count():
    for class_id,class_count in count_classes(r'C:\Projects\Home\Ingerdev\TestPad\redox\vocalfolds\dataset\annot'):
        print('class {0}: {1}'.format(class_id,class_count))

def test_enum_classes():
    mask_files = enum_files(r'C:\Projects\Home\Ingerdev\TestPad\redox\vocalfolds\dataset\annot')
    #mask_files = enum_files(r'C:\Projects\Home\Ingerdev\TestPad\redox\vocalfolds\dataset\img')
    print(mask_files)
    for class_id,class_count in count_classes(mask_files):
        print('class {0}: {1}'.format(class_id,class_count))
def test():
    #convert_masks(r'C:\Projects\Home\Ingerdev\TestPad\redox\vocalfolds\dataset\annot',
    #              r'C:\Projects\Home\Ingerdev\TestPad\redox\vocalfolds\dataset\sdfh')
    #test_filter()
    #test_compare()
    test_enum_classes()
    #test_count()
def test_compare():
    original_image_1 = skimage.data.imread((r"C:\Projects\Home\Ingerdev\TestPad"
                                          r"\redox\vocalfolds\dataset\img\patient2\seq6\0023_l.png"));
    mask_1 = skimage.data.imread((r"C:\Projects\Home\Ingerdev\TestPad"
                            r"\redox\vocalfolds\dataset\annot\patient2\seq6\0023_l.png"))
    original_image_2 = skimage.data.imread((r"C:\Projects\Home\Ingerdev\TestPad"
                                          r"\redox\vocalfolds\dataset\img\patient1\seq1\0039.png"));
    mask_2 = skimage.data.imread((r"C:\Projects\Home\Ingerdev\TestPad"
                            r"\redox\vocalfolds\dataset\annot\patient1\seq1\0039.png"))
    original_image_3 = skimage.data.imread((r"C:\Projects\Home\Ingerdev\TestPad"
                                            r"\redox\vocalfolds\dataset\img\patient2\seq7\0233.png"));
    mask_3 = skimage.data.imread((r"C:\Projects\Home\Ingerdev\TestPad"
                              r"\redox\vocalfolds\dataset\annot\patient2\seq7\0233.png"))
    print(np.unique(mask_1,return_counts=True))
    print(np.unique(mask_2,return_counts=True))
    fig, (o_1,m_1,o_2,m_2,o_3,m_3) = plt.subplots(6,1,figsize=(6,15));
    o_1.imshow(mask_1)
    m_1.imshow(skimage.color.label2rgb(mask_1,original_image_1,['red','yellow','yellow','yellow','yellow','yellow','gray','blue','cyan']))
    o_2.imshow(mask_2)
    m_2.imshow(skimage.color.label2rgb(mask_2,original_image_2,['red','yellow','yellow','yellow','yellow','yellow','gray','blue','cyan']))
    o_3.imshow(mask_3)
    m_3.imshow(skimage.color.label2rgb(mask_3,original_image_3,['red','yellow','yellow','yellow','yellow','yellow','gray','blue','cyan']))
    fig.show();

def test_filter():

    original_image = skimage.data.imread((r"C:\Projects\Home\Ingerdev\TestPad"
                                          r"\redox\vocalfolds\dataset\img\patient2\seq6\0023_l.png"));
    mask = skimage.data.imread((r"C:\Projects\Home\Ingerdev\TestPad"
                                r"\redox\vocalfolds\dataset\annot\patient2\seq6\0023_l.png"))

    print(np.unique(mask))
    print(mask)

    fig, (orig,mask_plot,inpaint_mask_plot,combined) = plt.subplots(4,1,figsize=(6,15));

    combined.imshow(skimage.color.label2rgb(mask,original_image,['black','red','yellow','gray','blue','cyan']))
    inpaint_mask = remove_class_inpaint_manual(mask,0)
    print(np.unique(inpaint_mask))
    inpaint_mask_plot.imshow(inpaint_mask)
    orig.imshow(original_image)
    mask_plot.imshow(mask)

    fig.show();

    #import sys;
# sys.stdin.readline();
