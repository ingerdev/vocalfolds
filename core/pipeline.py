import numpy as np
from core import data_util
from sklearn.base import BaseEstimator
from sklearn.utils.validation import check_consistent_length
from sklearn.utils import shuffle
from seglearn.transform import XyTransformerMixin
import numbers
from functools import reduce
import operator
import os

def enum_file_data(image_data_folder,mask_data_folder):
    x = []
    y = []

    for subdir, dirs, files in os.walk(image_data_folder):
        #note that os.path.join will interpret any subdirs started with / as
        #start of absolute path and will throw away anything it collected before

        for file in files:
            x.append(os.path.join(subdir, file))
            y.append(os.path.join(mask_data_folder,subdir[len(image_data_folder):].lstrip(os.sep),file))
    #print(*zip(x,y),sep='\n')
    return x,y

#def generate_data(x_raw,y_raw):
#    ImageDataGenerator

class ClassCounter(BaseEstimator):
     #2d-numpy array
     #rows are mapped to mask files 1-to-1
     #columns are class labels [0..max_class_label-1]
     #each cell contain pixels count labeled with column class
     unrolled_class_matrix_ = []
     class_statistics_ = []


     def fit(self, X, y):
        #check that X and y have correct shapes
        check_consistent_length(X,y)

        mask_class_data = [(data_util.enum_classes(mask_filename)) for mask_filename in y]

        #determine how much classes we have in the dataset
        class_numbers = max(max(i,key=lambda x:x[0]) for i in mask_class_data)[0] + 1

        #compose list of fixed-length list of per-class pixels count of every masks
        #and convert it to ndarray with shape (X[0],max_class_id)
        self.class_matrix_ = np.fromiter(reduce(operator.concat,[normalize_class_pixels_array(item,class_numbers)
                                  for item in mask_class_data]),int).reshape(X.shape[0],class_numbers)
        return self

#input tuple of two arrays ([unique_classes],[pixels count])
#return [0..max_classes-1] array filled with pixels count of every class
#for missed class labels it write 0
def normalize_class_pixels_array(mask_classes_list,class_numbers):
    classes_dict = dict(mask_classes_list)
    return [classes_dict[i] if classes_dict.get(i) != None else 0 for i in range(class_numbers)]


class Multiplicator(BaseEstimator,XyTransformerMixin):
    """Copy array values N times
    """
    def __init__(self,n_mult,shuffle=True,random_state=None):
        self.n_mult = n_mult
        self.shuffle = shuffle
        self.random_state = random_state

    def fit(self, X, y, **fit_params):
        return self

    def transform(self,X,y=None,sample_weight=None):
        #copy every entry in array n_mult times and return extended array
        multiplied_X = np.repeat(X,self.n_mult,axis=0)

        #shuffle items if necessary
        if (self.shuffle):
            return shuffle(multiplied_X,random_state=self.random_state),y,None

        return multiplied_X,y,None


class Devectorizer(BaseEstimator,XyTransformerMixin):
    """Convert data/test that contain row ids back to original filename values
    """
    def __init__(self,original_X,original_y):
        self.original_X = original_X
        self.original_y = original_y

    def fit(self,X,y, **fit_params):
        return self

    def transform(self, X, y = None, sample_weight=None, **fit_params):
        return self.original_X[X],self.original_y[X],None

class DumpClassifier(BaseEstimator):
    def __init__(self):
        return
    def fit(self,X,y, **fit_params):
        return self
    def score(self, X, y, **kwargs):
        print(len(X),len(np.unique(X)))

        return 0.5

class MaskVectorizer(BaseEstimator,XyTransformerMixin):
    """Transforms list of filenames to masks to vectors

    This transformer load mask images, determine which classes
    are present in every mask and build MxN binary array,
    where M is count of mask files and N is count of all
    classes found in the mask files
    """
    def __init__(self):
       return

    def fit(self,X,y):
        return self._fit(X,y)

    def fit_transform(self, X, y, sample_weight=None, **fit_params):
        #return same matrix both
        return self._fit(X,y).class_matrix_, self._fit(X,y).class_matrix_,None

    def _fit(self,X,y):
        #check that X and y have correct shapes
        check_consistent_length(X,y)

        mask_class_data = [(data_util.enum_classes(mask_filename)) for mask_filename in y]


        #determine how much classes we have in the dataset
        self.classes_count_ = max(max(i,key=lambda x:x[0]) for i in mask_class_data)[0] + 1

        #compose list of fixed-length list of per-class pixels count of every masks
        #and convert it to ndarray with shape (X[0],max_class_id)
        self.class_matrix_ = np.fromiter(reduce(operator.concat,[normalize_class_pixels_array(item,self.classes_count_)
                                                                 for item in mask_class_data]),int).reshape(X.shape[0],self.classes_count_)

        return self


#https://scikit-learn.org/stable/glossary.html#term-cv-splitter
#non-finished as skmultilearn.model_selection.IterativeStratification was found
class KFoldNaiveMultilabel:
    def __init__(self,n_splits=2, random_state=None, shuffle=False):
        if not isinstance(n_splits, numbers.Integral):
            raise ValueError('The number of folds must be of Integral type. '
                             '%s of type %s was passed.'
                             % (n_splits, type(n_splits)))
        n_splits = int(n_splits)

        if n_splits <= 1:
            raise ValueError(
                "k-fold cross-validation requires at least one"
                " train/test split by setting n_splits=2 or more,"
                " got n_splits={0}.".format(n_splits))

        if not isinstance(shuffle, bool):
            raise TypeError("shuffle must be True or False;"
                            " got {0}".format(shuffle))

        self.n_splits = n_splits
        self.random_state = random_state
        self.shuffle = shuffle

    def split(self,X,y):
        check_consistent_length(X,y)
        classes = np.count_nonzero(self.class_matrix_,axis=0)
        sorted_classes = sorted(zip(range(classes),classes),key=lambda t:t[1])

        #"mirror" rows index array
        rows_index = np.arange(y.shape[0])

        if (self.shuffle):
            np.random.seed(self.random_state)
            np.random.shuffle(rows_index)

    def get_n_splits(self):
        return self.n_splits

    def __gen_dataset_statistic(self,X,y):

        return



