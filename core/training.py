from model import vocalfolds,data_model
from keras_preprocessing.image import ImageDataGenerator
from core.pipeline import MaskVectorizer,Devectorizer,Multiplicator
from skmultilearn.model_selection import IterativeStratification
import numpy as np
import pandas as pd
from keras import callbacks
import matplotlib.pyplot as plt;
from core import data_generator,classes_weight_calculator
import skimage;
import os
from core.data_generator import imgaug_image_generator


def test_without_pipeline():
    X,y = vocalfolds.VocalFolds(r'..\dataset\img',
                     r'..\dataset\annot')

    weights = classes_weight_calculator.calculate_dataset_wide_weights(y,[0])
    #weights[4] /= 10 #override most weight and lower it 10 times
    weights = [0,1,5,20,40,5,20]
    #preprocessing stage
    mask_vec = MaskVectorizer()

    #convert raw 1d X/y filenames to 2d class labels array
    _,class_y,_ = mask_vec.fit_transform(X,y)
    kfold_splitter = IterativeStratification(n_splits=10,random_state=6)

    multiplicator = Multiplicator(n_mult=50,shuffle=True,random_state=505)
    devectorizer = Devectorizer(X,y)

    X_index = np.arange(len(X))
    early_stopping = callbacks.EarlyStopping(monitor='val_loss', patience=1, verbose=0, mode='auto')
    model_checkpoint = callbacks.ModelCheckpoint(r'..\weights.h5', monitor='val_loss', save_best_only=True)

    load_model = True
    model_filename = r'..\weights.h5'
    if load_model and os.path.isfile(model_filename):
        keras_model = data_model.load_trained_model(model_filename,weights)
    else:
        keras_model = data_model.gen_model_crossentropy_loss(weights)

    #from keras.utils import plot_model
    #plot_model(keras_model, to_file='model.png')
    test_mode = False
    train_random_state=None if test_mode else 17
    test_random_state=None if test_mode else 18

    start_fold = 0 #0..9
    batch_size = 4
    epochs = 20

    print("loading images...")
    X_images = {}
    y_images = {}
    import cv2
    for x_filename,y_filename in zip(X,y):
        x_image = skimage.data.imread(x_filename)#cv2.imread(x_filename)
        y_image = skimage.data.imread(y_filename)#cv2.imread(y_filename)
        X_images[x_filename] = x_image
        y_images[y_filename] = y_image
    print("images loaded.")

    for train_idx,test_idx in kfold_splitter.split(X_index,class_y):
        X_p_train,y_p_train = preprocess_data_chunk(X_index[train_idx],multiplicator,devectorizer)
        X_p_test,y_p_test = preprocess_data_chunk(X_index[test_idx],multiplicator,devectorizer)


        if (start_fold)>0:
            start_fold -= 1
            continue

        train_flow =  imgaug_image_generator(X_p_train,y_p_train,X_images,y_images,n_classes=7,batch_size=batch_size,random_state=train_random_state)
        test_flow = imgaug_image_generator(X_p_test,y_p_test,X_images,y_images,n_classes=7,batch_size=batch_size,random_state=test_random_state)

        if (test_mode):
            #test visualize augmented data
            xx,yy = next(train_flow)
            #test_show_augmented_batch_images(X_p_train,y_p_train,xx,yy)
            continue

        keras_model.fit_generator(train_flow,steps_per_epoch=len(X_p_train)/70, epochs=epochs,
                                  validation_data=test_flow,
                                  validation_steps=len(X_p_test)/70,
                                  callbacks = [model_checkpoint])#,class_weight={0:0.0,1:0.2,2:0.2,3:0.2,4:0.5,5:0.2,6:0.2})
        train_random_state+=2
        test_random_state+=2


def test_show_augmented_batch_images(X_p_train,y_p_train,xx,yy):

    for train_x_filename,train_y_filename,x,y in zip(X_p_train,y_p_train,xx,yy):
        #x_batch,y_batch = next(flow)
        restored_mask = data_generator.reshape_prediction_array_to_mask_image(y,(512,512))
        fig, (train_x_plot,train_y_plot,image_plot,mask_plot) = plt.subplots(4,1,figsize=(6,15));
        train_x_plot.imshow(skimage.data.imread(train_x_filename))
        train_y_plot.imshow(skimage.data.imread(train_y_filename))
        image_plot.imshow(x)
        mask_plot.imshow(restored_mask)
        fig.show();



def preprocess_data_chunk(X,mult,dvc):
    X_index_multiplied,_,_ = mult.transform(X)
    X_transformed,y_transformed,_ = dvc.transform(X_index_multiplied)

    return X_transformed,y_transformed

def __image_flow(X,y,batch_size=6,random_state=None):
    data_gen_args = {
        'horizontal_flip':True,
        'vertical_flip':True,
        'width_shift_range':0.1,
        'height_shift_range':0.1,
        'zoom_range':0.1,
        'rotation_range':10,
        'rescale':1.0/255
    }
    shuffle = True if random_state!=None else False
    X_df = pd.DataFrame({'filename':X})
    x_gen = ImageDataGenerator(**data_gen_args)
    x_flow = x_gen.flow_from_dataframe(X_df,target_size=(512,512),
                                       batch_size=batch_size,shuffle = shuffle,seed=random_state,drop_duplicates=False,class_mode=None)

    y_df = pd.DataFrame({'filename':y})
    y_gen = ImageDataGenerator(**data_gen_args)
    y_flow = y_gen.flow_from_dataframe(y_df,target_size=(512,512),color_mode='grayscale',
                                       batch_size=batch_size,shuffle = shuffle,seed = random_state,drop_duplicates=False,class_mode=None)
    return zip(x_flow,y_flow)


def test_dataset_valid_images(files_list):
    import cv2 as cv
    for imgname in files_list:
        img = cv.imread(imgname)
        if img is None:
            print(imgname)

def main():
    test_without_pipeline()


if __name__ == "__main__":
    main()